var NativeJavascript = function () {
  var _this = this;
  _this.apiKey = 'ece76db396854017e22478044612680d';
  _this.baseUrl = 'https://api.flickr.com/services/rest/?api_key=' + _this.apiKey + '&format=json&nojsoncallback=1&sort=relevance';
  _this.result = [];
  _this.selectedImages = [];
  _this.searchResultDiv = document.getElementById('searchResult');

  _this.searchFlickr = function() {
    _this.selectedImages = [];
    _this.nrOfSelectedImages.className += ' noshow';
    document.getElementById('previewgalleryDescription').value = '';
    document.getElementById('previewgalleryTitle').value = '';
    document.getElementById('jumbotron').style.display = 'block';
    document.getElementById('galleryDiv').style.display = 'none';
    document.getElementById('fullSizeImage').innerHTML = '';
    document.getElementById('galleryDiv').className += ' noshow';
    var searchstring = document.getElementById('flickrSearchString').value;
    if (searchstring === '') {
      alert("You can't search for nothing here!");
    } else {
      var apiurl = _this.baseUrl + '&method=flickr.photos.search&text=' + searchstring;
      var xhrResponse = _this.createXMLHttpRequest(apiurl, function(data) {
        if (data !== false) {
          var searchResultDiv = document.getElementById('searchResult');
          searchResultDiv.innerHTML = '';
          for (var i = 0; i < data.photos['photo'].length; i++) {
            searchResultDiv.appendChild(_this.imageTemplate(data.photos['photo'][i], '', '_m.jpg', 'Click to add image to your collection'));
            _this.result.push(data.photos['photo'][i]);
          }
        } else {
          callback(false);
        }
      }, 'GET');
    }
  }

  _this.selectImageFunctionality = function(e) {
    if (e.target.tagName == 'IMG') {
      var id = e.target.getAttribute('id');
      var img = document.getElementById(id);
      var index = _this.selectedImages.map(function(i) {
        return i.id;
      }).indexOf(id);
      if (index >= 0) {
        _this.selectedImages.splice(index, 1);
        img.classList.remove('selected');
        _this.selectedImages.length > 0 ? false : _this.nrOfSelectedImages.className += ' noshow';
      } else {
        _this.selectedImages.push({id});
        _this.nrOfSelectedImages.classList.remove('noshow');
        img.className += ' selected';
      }
      _this.nrOfSelectedImages.childNodes[1].innerHTML = _this.selectedImages.length;
    }
  }

  _this.createGallery = function() {
    var galleryDiv = document.getElementById('galleryDiv');
    galleryDiv.style.display = 'block';
    var searchResult = document.getElementById('searchResult');
    searchResult.innerHTML = '';
    var gallerySpan = document.getElementById('gallerySpan');
    gallerySpan.innerHTML = '';
    document.getElementById('galleryTitle').innerHTML = document.getElementById('previewgalleryTitle').value;
    document.getElementById('galleryDescription').innerHTML = document.getElementById('previewgalleryDescription').value;
    for (var i = 0; i < _this.selectedImages.length; i++) {
      var index = _this.getImageData(_this.result, _this.selectedImages[i]);
      gallerySpan.appendChild(_this.imageTemplate(_this.result[index], '_G', '_m.jpg', 'Click to see full size'));
    }
    _this.modal.style.display = 'none';
    _this.nrOfSelectedImages.className += ' noshow'
  }

  _this.viewOneGalleryImage = function(e) {
    if (e.target.tagName == 'IMG') {
      var galleryDiv = document.getElementById('galleryDiv');
      galleryDiv.style.display = 'none';
      var id = e.target.getAttribute('id').replace('_G', '');
      var viewImage = document.getElementById('fullSizeImage');
      viewImage.innerHTML = '';
      var index = _this.result.findIndex(i => i.id === id);
      var img = _this.imageTemplate(_this.result[index], '_F', '_b.jpg', 'Click to return to your gallery');
      img.className += ' fullSizeImage';
      viewImage.appendChild(img);
    }
  };

  _this.previewGallery = function(e) {
    document.getElementById('jumbotron').style.display = 'none';
    _this.modal.style.display = 'block';
    var selectedImagesDiv = document.getElementById('selectedImages');
    selectedImagesDiv.innerHTML = '';
    for (var i = 0; i < _this.selectedImages.length; i++) {
      console.log(_this.selectedImages[i]);
      var index = _this.getImageData(_this.result, _this.selectedImages[i]);
      selectedImagesDiv.appendChild(_this.imageTemplate(_this.result[index], '_S', '_m.jpg', 'Click to remove image from your collection'));
    }
  }

  _this.removeImageFromSelectedCollection = function (e) {
    if (e.target.tagName == 'IMG') {
      var id = e.target.getAttribute('id').replace('_S', '');
      var img = document.getElementById(id);
      img.classList.remove('selected');
      var index = _this.getImageData(_this.selectedImages, id);
      _this.selectedImages.splice(index, 1);
      e.target.parentNode.removeChild(e.target);
      _this.nrOfSelectedImages.childNodes[1].innerHTML = _this.selectedImages.length;
      if (_this.selectedImages.length < 1) {
        _this.modal.style.display = 'none';
        _this.nrOfSelectedImages.className += ' noshow'
      }
    }
  }

  _this.getImageData = function(haystack, needle) {
    return haystack.map(function(e) {
      return e.id;
    }).indexOf(needle.id);
  }

  _this.imageTemplate = function(imgData, idappend, urlappend, title) {
    var img = document.createElement('img');
    var imgurl = 'https://farm' + imgData.farm + '.staticflickr.com/' + imgData.server + '/' + imgData.id  + '_' + imgData.secret + urlappend;
    var imgid = imgData.id + idappend;
    img.setAttribute('id', imgid);
    img.setAttribute('src', imgurl);
    img.setAttribute('title', title);
    img.setAttribute('class', 'img-thumbnail img-responsive');
    return img;
  }

  _this.createXMLHttpRequest = function(apiurl, callback, method) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if(xhr.readyState === 4 && xhr.status === 200) {
        content = xhr.responseText;
        if(content != '' && (content)) {
            callback(JSON.parse(content));
        } else {
            callback(false);
        }
        return JSON.parse(this.response);
      }
    }
    xhr.open(method, apiurl, true);
    xhr.send(null);
  }

  window.onload = function() {
    _this.resultDiv = document.getElementById('searchResult');
    _this.searchButton = document.getElementById('searchButton');
    _this.createGalleryButton = document.getElementById('createGallery');
    _this.previewGalleryButton = document.getElementById('previewGallery');
    _this.modalSpan = document.getElementById('modalClose');
    _this.imageModalSpan = document.getElementById('imageModalClose');
    _this.modal = document.getElementById('previewGalleryModal');
    _this.selectedImagesCollection = document.getElementById('selectedImages');
    _this.nrOfSelectedImages = document.getElementById('selectedForGallerySpan');
    _this.gallerySpan = document.getElementById('gallerySpan');
    _this.fullSizeImage = document.getElementById('fullSizeImage');
    _this.navHome = document.getElementById('home');
    window.addEventListener('click', function(e) {
      if (e.target == _this.modal) {
        _this.modal.style.display = 'none';
      } else if (e.target == _this.viewImageModal) {
        _this.viewImageModal.style.display = 'none';
      }
    }, false);
    _this.navHome.addEventListener('click', function(e) {
      e.preventDefault();
      location.reload(true);
    }, false);
    _this.fullSizeImage.addEventListener('click', function(e) {
      e.preventDefault();
      document.getElementById('galleryDiv').style.display = 'block';
      document.getElementById('fullSizeImage').innerHTML = '';
    }, false);
    _this.gallerySpan.addEventListener('click', function(e) {
      e.preventDefault();
      _this.viewOneGalleryImage(e);
    }, false);
    _this.resultDiv.addEventListener('click', function(e) {
      e.preventDefault();
      _this.selectImageFunctionality(e);
    }, false);
    _this.searchButton.addEventListener('click', function(e) {
      e.preventDefault();
      _this.searchFlickr();
    }, false);
    _this.previewGalleryButton.addEventListener('click', function(e) {
      e.preventDefault();
      _this.previewGallery(e);
    }, false);
    _this.modalSpan.addEventListener('click', function(e) {
      _this.modal.style.display = 'none';
    }, false);
    _this.imageModalSpan.addEventListener('click', function(e) {
      _this.modal.style.display = 'none';
    }, false);
    _this.createGalleryButton.addEventListener('click', function(e) {
      _this.createGallery();
    }, false);
    _this.selectedImagesCollection.addEventListener('click', function(e) {
      _this.removeImageFromSelectedCollection(e);
    }, false);
  }
}

var flickrAPIInteraction = new NativeJavascript();
